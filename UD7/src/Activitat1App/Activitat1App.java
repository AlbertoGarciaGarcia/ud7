package Activitat1App;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class Activitat1App {

	public static void main(String[] args) {
		
		String alumnos = JOptionPane.showInputDialog(null,"Introduce la cantidad de alumnos que quieres calcular su nota"); // Pedirmeos al ususario la cantidad de alumnos a crear
		int alumnosInt = Integer.parseInt(alumnos); // Lo pasamos a Int
	
	
		
		Hashtable<String,String> tablaNotas = new Hashtable<String, String>(); // Creamos el hashtable
		
	
		introducirNotas(tablaNotas,alumnosInt); // Llamamos a la funcion para introducir notas pasando por parametro el hashtalbe a rellenar y el numero de alumnos
		mostrarDatos(tablaNotas); // Imprimimos el hastable deseado
		
		
		
		
		
		
		
		
		
		
		

	
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public static int calculoMedia() {
		int media = 0;
		for (int i = 1; i < 6; i++) { // Pondremos hasta 5 porque en este ejemplo seran 5 notas 
			String nota = JOptionPane.showInputDialog(null,"Introduce la nota de la asignatura : " + i); // Pedirmeos al ususario la cantidad de alumnos a crear
			int notaInt = Integer.parseInt(nota); // Lo pasamos a Int
			
			media = media + notaInt;  // Vamos sumando las notas
					
			
		}
		media = (media/5); // Calculamos la media
		return media; //La devolvemos
	}
	
	
	public static void mostrarDatos(Hashtable tablaNotas) {
Enumeration<String> llaves = tablaNotas.keys(); // Creamos una enumeracion para las llaves
		
		Enumeration<String> enumeration = tablaNotas.elements(); // Y otra para los elementos
		while (enumeration.hasMoreElements()) { // Mientras tenga elementos
			System.out.println(" " + "El alumno con ID: " + llaves.nextElement() + " tiene una media de " + enumeration.nextElement());  // Imprimimos
		}
	}
	
	public static void introducirNotas(Hashtable tablaNotas, int alumnosInt) {
		for (int i = 0; i <  alumnosInt; i++) { // Por cada alumno que haya
			String alumnoNombre = JOptionPane.showInputDialog(null,"Introduce la ID del Alumno"); // Introducimos la ID
			int notaMedia = calculoMedia(); // Calculamos su media
			String notaString = String.valueOf(notaMedia); 
			tablaNotas.put(alumnoNombre,notaString); // Y la introducimos en el hashtable
			
		}
		
		
	}
	
	
	

}
