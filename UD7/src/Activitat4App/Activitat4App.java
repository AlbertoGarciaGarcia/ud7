package Activitat4App;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class Activitat4App {

	public static void main(String[] args) {
		
		
		String decisionUsuario;

		Hashtable<String,Double> articuloLista = new Hashtable<String,Double>(); // Creamos el hashtable
		
		introducirDiezPrimerosProductos(articuloLista); // Llenamos el hashtable
		
		
		do {
			JOptionPane.showMessageDialog(null,"Que operacion quieres realizar?"); // Hacemos un menu para preguntar las opciones
			decisionUsuario = JOptionPane.showInputDialog(null,"1 = A�adir 2 = Mostrar uno concreto 3 = Mostrar Todos, 4 = Supermercado Escribe No para salir"); // Ense�amos las opciones 	
			
			switch (decisionUsuario) {
			case "1":
				a�adirProducto(articuloLista); // Llamamos a la funcion de a�adir
				break;
			case "2":	
				mostrarProducto(articuloLista); // Llamamos a la funcion de mostrar un producto en especifico
				break;
			case "3":
				mostrarDatos(articuloLista); // Llalamos a la funcion de mostrar todo
				break;
			case "4":
				carritoCompra(articuloLista);
				break;
			}
		} while(!decisionUsuario.equals("No"));
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
			
	}
	
	public static void carritoCompra(Hashtable<String, Double> listaCompra) {
		JOptionPane.showMessageDialog(null,"Bievenido al supermercado");
		String productos = JOptionPane.showInputDialog(null,"Introduce la cantidad de productos comprados");
		int productosInt = Integer.parseInt(productos); // Pedimos la cantidad de productos
		double totalPrecio = 0;
		ArrayList<String[]> listaProductos = new ArrayList<>(); // Creamos el array list
		for (int i = 0; i < productosInt; i++) { // Ejectuamos los procesos como tantos productos tengamos
			String[] arrayProductos = introducirProducto(listaCompra); // Creamos el array para no tener que ejecutar la funcion multiplees veces
			listaProductos.add(arrayProductos); // A�adimos el array al arrayList
			totalPrecio = totalPrecio + calcularTotal(arrayProductos); // Sumamos el total del precio
			
		}
		
		String cambioCliente = JOptionPane.showInputDialog(null,"Introduce la cantidad a pagar");
		double cambioClienteDouble = Double.parseDouble(cambioCliente); // Pedimos el cambio
		
		double cambio = cambioClienteDouble - totalPrecio; // Lo calculamos
	
		
		
		
		
		
		for (int i = 0; i < productosInt; i++) {
			System.out.println("El nombre del producto es " + listaProductos.get(i)[0]); // Impriremos cada producto de acuerdo a su posicion dentro del array
			System.out.println("El tipo de IVA es  " + listaProductos.get(i)[1]);
			System.out.println("El precio bruto es  " + listaProductos.get(i)[2]);
			System.out.println("El precio neto es  " + listaProductos.get(i)[3]);
			
			System.out.println("                                                          ");

		}
		
		
		
		System.out.println("El precio total es " + totalPrecio + " y tienes un cambio de " +  cambio); // Calculamos el cambio del prodcuto
	}
	
	
	
	public static String[] introducirProducto(Hashtable<String, Double> listaProductos) {
		String nombreProducto;
		
		do {
			nombreProducto = JOptionPane.showInputDialog(null,"Introduce el nombre del producto");
			if(listaProductos.get(nombreProducto) == null) { // Aqui comprobaremos que el producto este en la lista de stock
				nombreProducto = null;
			}
		}while(nombreProducto == null); // Hasta que no seleccione el correcto no seguira
		String[] arrayProducto = new String[4]; // Crearemos un array con 4 posiciones ya que cada producto dispone de las mismas caracteristicas
		arrayProducto[0] = nombreProducto; // Iremos asignandno el valor a la posicion adiente
		String ivaProducto = JOptionPane.showInputDialog(null,"Introduce el IVA del producto");
		int ivaProductoInt = Integer.parseInt(ivaProducto); 
		arrayProducto[1] = ivaProducto;
	
		String precioProducto = String.valueOf(listaProductos.get(nombreProducto)); 
		arrayProducto[2] = precioProducto;
		
		
	
		double precioProductoInt = Double.parseDouble(precioProducto); // Cogeremos el producto de la base de datos
		double netoProducto = precioProductoInt + ((precioProductoInt/100) * ivaProductoInt); // Calculamos el precio junto al iva
		
		String netoString = String.valueOf(netoProducto);
		arrayProducto[3] = netoString;
		
		return arrayProducto;
		
		
		
	}
	
	public static double calcularTotal(String[] array) {
		
		String total = array[3];
		double totalDouble = Double.parseDouble(total); // Hacemos que calcule el total pasandolo a double 
		
		
		return totalDouble;
	}
	
	public static void introducirDiezPrimerosProductos(Hashtable<String,Double>listaArticulos) { // Ponemos un productos por nuestra cuenta
		listaArticulos.put("CocaCola",1.3);
		listaArticulos.put("Pepsi",1.6);
		listaArticulos.put("Jamon",2.9);
		listaArticulos.put("Tortilla",4.5);
		listaArticulos.put("Manzana",1.5);
		listaArticulos.put("Ventilador",20.4);
		listaArticulos.put("Helados",2.3);
		listaArticulos.put("Patata",0.5);
		listaArticulos.put("Perfume",6.7);
		listaArticulos.put("Bolsa",0.3);
		
		
		
	
	}
	
	
	public static void a�adirProducto(Hashtable<String, Double> listaArticulos) {
		
		
		
		String nombreProducto = JOptionPane.showInputDialog(null," A�ade el nombre del producto nuevo"); 	
		String precioProducto = JOptionPane.showInputDialog(null," A�ade el precio del producto nuevo"); 	
		double productosDouble = Double.parseDouble(precioProducto);  // Pedimos el nombre y el precio
		
		
		listaArticulos.put(nombreProducto,productosDouble); // Y lo a�adimos 
		
		
		
		
		
		
	
		
	}
	
	
	
	public static void mostrarProducto(Hashtable<String, Double> listaArticulos) { 
		String infoProducto = JOptionPane.showInputDialog(null," De que producto quieres saber la informaci�n"); 	// Pedimos la informacion del producto
		
		System.out.println("El producto " + infoProducto + " dispone de un precio de : " + listaArticulos.get(infoProducto)); // Preguntamos la informacion del producto
		
	}
	
	
	public static void mostrarDatos(Hashtable<String, Double> tablaNotas) {
		Enumeration<String> llaves = tablaNotas.keys(); // Creamos una enumeracion para las llaves
				
				Enumeration<Double> enumeration = tablaNotas.elements(); // Y otra para los elementos
				while (enumeration.hasMoreElements()) { // Mientras tenga elementos
					System.out.println(" " + "El producto : " + llaves.nextElement() + " tiene un precio  de " + enumeration.nextElement());  // Imprimimos
				}
			}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	 
}
	
